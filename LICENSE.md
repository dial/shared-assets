# License

Unless otherwise indicated, this content by the Digital Impact Alliance at United Nations Foundation, collectively known as "DIAL Shared Assets", is licensed under a Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.